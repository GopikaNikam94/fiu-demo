export interface ConsentRequest {
  custId: String
  consentPurposeCode: String
  consentDescription: String
  dateTimeRangeFrom: any
  dateTimeRangeTo: Date
}

export interface SubmitConsentRequest {
  custId: String
  consentDescription: String
  templateName: String
  aaId: String
}