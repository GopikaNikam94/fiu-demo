import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from 'src/app/services/config/config-service';
import { Observable , throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { WebviewEncryptRequest, WebViewEncryptedRequest } from './webview-encrypt-request';
import { WebHeader } from '../web-header';
import { WebRequest } from '../web-request';

@Injectable({
  providedIn: 'root'
})
export class WebviewEncryptRequestService {

  public apiBaseUrl: string; channelId: string;
  
  constructor(public configService: ConfigService,
    private http: HttpClient) { 
      this.apiBaseUrl = this.configService.apiBaseUrl;
      this.channelId = this.configService.channelId;
    }

    webviewEncryptRequest(webviewEncryptRequest : WebviewEncryptRequest): Observable<WebviewEncryptRequest>{
      const header: WebHeader = {
        rid: localStorage.getItem("messageID"),
        ts: new Date(),
        channelId: this.channelId
      };
      const request: WebRequest = {
        header: header,
        body: webviewEncryptRequest
      };
      return this.http.post<WebviewEncryptRequest>(this.apiBaseUrl + '/Webview/Encrypt',request).pipe(retry(1),
      catchError(this.handleError));
    }

    decryptWebviewRequest(webViewEncryptedRequest : WebViewEncryptedRequest): Observable<WebViewEncryptedRequest>{
      const header: WebHeader = {
        rid: localStorage.getItem("messageID"),
        ts: new Date(),
        channelId: this.channelId
      };
      const request: WebRequest = {
        header: header,
        body: webViewEncryptedRequest
      };
      return this.http.post<WebViewEncryptedRequest>(this.apiBaseUrl + '/Webview/Decrypt',request).pipe(retry(1),
      catchError(this.handleError));
    }

    handleError(error) {
      let errorMessage = '';
      if (error.error instanceof ErrorEvent) {
        // client-side error
        errorMessage = `Error: ${error.error.message}`;
      } else {
        // server-side error
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      if(error.status == 401){
        window.alert("JWT expired!");
        window.location.href = '/login'
      }
      return throwError(errorMessage);
     }
}