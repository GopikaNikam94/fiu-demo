import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ConfigService } from 'src/app/services/config/config-service';
import { SubmitConsentRequest } from 'src/app/services/consent-request/consent-request';
import { ConsentRequestService } from 'src/app/services/consent-request/consent-request.service';
import { UserLoginService } from 'src/app/services/user-login/user-login.service';
import { UserLogin } from 'src/app/services/user-login/user-login';
import { Router } from '@angular/router';
import { WebviewEncryptRequest } from 'src/app/services/webview-encrypt-request/webview-encrypt-request';
import { WebviewEncryptRequestService } from 'src/app/services/webview-encrypt-request/webview-encrypt-request.service';
import * as uuid from 'uuid';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-iframe',
  templateUrl: './iframe-component.html',
  styleUrls: ['./iframe-component.css',
    '../../../assets/icofont/css/aa-onboarding-common.scss']
})
export class IframeComponent implements OnInit {

  public onboardingUrl: String; username: String; password: String; custId: string; fiuId: string;
  userPostfixName: string; redirectUrl: string; passPhrase: string; templateName: string;
  consentDescription: string; consentHandleId: string; aaId: string
  public response: any; consentDetail: any; loanAmount: any; amount: any;
  authorizationToken: any; fiuDemoPageUrl: any
  public from: Date;
  public applyLoanForm: FormGroup;
  public values = null;
  
  iframeURL: SafeResourceUrl;
  displayFrame: boolean = false;
  iframe: Window;


  constructor(public formBuilder: FormBuilder, public configService: ConfigService,
    public router: Router, public consentRequestService: ConsentRequestService,
    public userLoginService: UserLoginService,
    public webviewEncryptRequestService: WebviewEncryptRequestService, private sanitizer: DomSanitizer) {

    this.onboardingUrl = this.configService.onboardingUrl;
    this.username = this.configService.username;
    this.password = this.configService.password;
    this.fiuId = this.configService.fiuId;
    this.userPostfixName = this.configService.userPostfixName;
    this.redirectUrl = this.configService.redirectUrl;
    this.templateName = this.configService.templateName;
    this.consentDescription = this.configService.consentDescription;
    this.aaId = this.configService.aaId;

    this.applyLoanForm = formBuilder.group({
      'name': [null, Validators.compose([Validators.required, Validators.pattern(".*\\S.*[a-zA-z]")])],
      'mobile': [null, Validators.compose([Validators.required, Validators.pattern('[6-9]\\d{9}')])],
      'loanAmount': [null, Validators.compose([Validators.required, Validators.pattern(".*\\S.*[0-9 ]")])],
      'aaId': [null, Validators.compose([Validators.pattern('^[a-zA-Z0-9._-]{6,30}$')])]
    });

    sessionStorage.clear();
    this.authorizationTokenLogin();
  }

  ngOnInit() {}

  authorizationTokenLogin() {
    const userLogin: UserLogin = {
      userId: this.username,
      password: this.password
    }

    console.log("Authorization Username: " + this.username);
    console.log("Authorization Password: " + this.password);

    this.userLoginService.userLogin(userLogin).subscribe(data => {
      console.log("UserLogin Response: ", JSON.stringify(data['body']));
      this.response = data['body'];
      this.authorizationToken = this.response['token'];
      sessionStorage.setItem('authorizationToken', this.authorizationToken);
    })
  }

  submitConsentRequest() { 
    if (sessionStorage.getItem("authorizationToken") != null) {
      if (this.applyLoanForm.get('aaId').value == null 
          || this.applyLoanForm.get('aaId').value == '@finvu'
          || this.applyLoanForm.get('aaId').value == '') {
          this.custId = this.applyLoanForm.get('mobile').value + this.userPostfixName
      } else {
          this.custId = this.applyLoanForm.get('aaId').value + this.userPostfixName
      }

      const submitConsentRequest: SubmitConsentRequest = {
        custId: this.custId,
        consentDescription: this.consentDescription,
        templateName: this.templateName,
        aaId: this.aaId
      }

      sessionStorage.setItem("aaId",this.aaId);
      console.log("FIU Consent Request: ", submitConsentRequest);
      this.consentRequestService.submitConsentRequest(submitConsentRequest).subscribe(data => {
        if (data['errors']) {
          var data1 = data['errors'];
          for (let key in data1) {
            var result = data1[key];
            console.log("result : " + JSON.stringify(result));
            if (result.errorCode == 400) {
              alert("Customer address is not valid mobile number")
              this.router.navigate(['/iframe-component'])
            } else {
              alert("Something went wrong.")
            }
          }
        } else {
          console.log("FIU Consent Request Response: ", JSON.stringify(data['body']));
          this.response = data['body'];
          this.consentHandleId = this.response['consentHandle'];
          this.webviewEncryptRequest();
        }
      })
    } else {
      this.router.navigate(['/iframe-component']);
      console.log("Authorization token is null");
      sessionStorage.clear();
    }
  }

  webviewEncryptRequest() {
    if (sessionStorage.getItem("authorizationToken") != null) {
      const webviewEncryptRequest: WebviewEncryptRequest = {
        txnId: uuid.v4(),
        sessionId: "asd12345",
        srcRef: this.consentHandleId,
        userId: this.custId,
        redirectUrl: this.redirectUrl,
        fiuId: this.fiuId,
      }

      console.log("FIU Webview Encrypt Request: ", JSON.stringify(webviewEncryptRequest));
      sessionStorage.setItem('custId', this.custId);
      sessionStorage.setItem('consentHandleId', this.consentHandleId);

      this.webviewEncryptRequestService.webviewEncryptRequest(webviewEncryptRequest).subscribe(data => {
        console.log("FIU Webview Encrypt Request Response: ", JSON.stringify(data['body']));
        this.response = data['body'];  
        if(this.response == null 
             || this.response['encryptedRequest'] == null 
             || this.response['encryptedRequest'] == "" 
             || this.response['encryptedRequest'] == "undefined"
             || this.response['requestDate'] == null 
             || this.response['requestDate'] == "" 
             || this.response['requestDate'] == "undefined"
             || this.response['encryptedFiuId'] == null
             || this.response['encryptedFiuId'] == ""
             || this.response['encryptedFiuId'] == "undefined") {
            alert("Something went wrong!");
            console.log("FIU Webview Encrypt Response: ", this.response);
            sessionStorage.clear();
            this.router.navigate(['iframe-component']);
        } else {
            this.iframeURL = this.sanitizer.bypassSecurityTrustResourceUrl(this.onboardingUrl + "?ecreq=" + this.response['encryptedRequest'] + "&" + "reqdate=" + this.response['requestDate'] + "&" + "fi=" + this.response['encryptedFiuId']);
            window.addEventListener("message", this.receivemessage.bind(this), false);
            this.displayFrame = true;
        }
      })
    } else {
      this.router.navigate(['/iframe-component']);
      console.log("Authorization token is null");
      sessionStorage.clear();
    }
  }

  receivemessage(evt:any){
    this.displayFrame = false;
    console.log(JSON.stringify(evt.data))
    sessionStorage.setItem("object", evt.data);
    if(evt.data.status == 'ACCEPT' || evt.data.status == 'DENY') {
      this.router.navigate(['/loan-eligibility']);
    }
  }

  submitForm() {
    this.markFormTouched(this.applyLoanForm);
    if (this.applyLoanForm.valid) {
      // You will get form value if your form is valid
      //var formValues = this.applyLoanForm.getRawValue;
      this.onNextClick();
    } else { }
  };

  markFormTouched(group: FormGroup | FormArray) {
    Object.keys(group.controls).forEach((key: string) => {
      const control = group.controls[key];
      if (control instanceof FormGroup || control instanceof FormArray) { control.markAsTouched(); this.markFormTouched(control); }
      else { control.markAsTouched(); };
    });
  };

  onNextClick() {
    this.submitConsentRequest();
  }

  keyPress(event: any) {
    const pattern =/[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  printNo() {
    this.amount = Number(this.applyLoanForm.get('loanAmount').value).toLocaleString('en-GB');
    sessionStorage.setItem('loanAmount', this.amount);
  }
}