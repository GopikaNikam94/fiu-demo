import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IframeComponent } from './aa-onboarding-layout/iframe-component/iframe-component';

const appRoutes: Routes = [
  { 
    path: '', 
    redirectTo: 'onboarding-layout', 
    pathMatch: 'full' 
  },
  {
    path: 'iframe-component', 
    component: IframeComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
